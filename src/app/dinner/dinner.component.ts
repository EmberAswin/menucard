import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dinner',
  templateUrl: './dinner.component.html',
  styleUrls: ['./dinner.component.scss']
})
export class DinnerComponent implements OnInit {
  ToggleNav() {
    const nav = document.getElementById("navs");
    nav?.classList.toggle("hide");
  }
  constructor() { }

  ngOnInit(): void {
  }

}
