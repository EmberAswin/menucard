import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lunch',
  templateUrl: './lunch.component.html',
  styleUrls: ['./lunch.component.scss']
})
export class LunchComponent implements OnInit {
  ToggleNav() {
    const nav = document.getElementById("navs");
    nav?.classList.toggle("hide");
  }
  constructor() { }

  ngOnInit(): void {
  }

}
