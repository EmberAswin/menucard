import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {
  title = 'menucard';

  ToggleNav() {
    const nav = document.getElementById("navs");
    nav?.classList.toggle("hide");
  }

  constructor() { }

  ngOnInit(): void {
  }

}
