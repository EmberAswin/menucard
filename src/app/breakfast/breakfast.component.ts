import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-breakfast',
  templateUrl: './breakfast.component.html',
  styleUrls: ['./breakfast.component.scss']
})
export class BreakfastComponent implements OnInit {
  title = 'menucard';

  ToggleNav() {
    const nav = document.getElementById("navs");
    nav?.classList.toggle("hide");
  }

  constructor() { }

  ngOnInit(): void {
  }

}
