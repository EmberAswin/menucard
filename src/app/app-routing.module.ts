import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BreakfastComponent } from './breakfast/breakfast.component';
import { DinnerComponent } from './dinner/dinner.component';
import { LunchComponent } from './lunch/lunch.component';
import { MainPageComponent} from './main-page/main-page.component'

const routes: Routes = [
  {path:'',component:MainPageComponent},

  {path:'breakfast',component:BreakfastComponent},
  {path:'lunch',component:LunchComponent},
  {path:'dinner',component:DinnerComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
